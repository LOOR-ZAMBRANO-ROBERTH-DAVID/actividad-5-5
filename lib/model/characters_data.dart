import 'package:cartoons_flutter/model/character.dart';

final characters = [
  Character(
    name: 'Albert Alemany',
    age: 28,
    image: 'images/albert.jpg',
    jobTitle: 'Flutter Developer',
    stars: 4.3,
  ),
  Character(
    name: 'Gerard Guasch',
    age: 21,
    image: 'images/gerard.jpg',
    jobTitle: 'Android Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Ignasi Isern',
    age: 33,
    image: 'images/ignasi.jpg',
    jobTitle: 'iOS Developer',
    stars: 4.9,
  ),
  Character(
    name: 'Meritxell Maimó',
    age: 29,
    image: 'images/meritxell.jpg',
    jobTitle: 'React Native Developer',
    stars: 4.1,
  ),
  Character(
    name: 'Mònica Moragues',
    age: 24,
    image: 'images/monica.jpg',
    jobTitle: 'Web Developer',
    stars: 3.5,
  ),
  Character(
    name: 'Pol Pitarch',
    age: 19,
    image: 'images/pol.jpg',
    jobTitle: 'UI Designer',
    stars: 2.9,
  ),
  Character(
    name: 'Raquel Reixach',
    age: 35,
    image: 'images/raquel.jpg',
    jobTitle: 'Backend Developer',
    stars: 3.8,
  ),
  Character(
    name: 'Rebeca Roig',
    age: 31,
    image: 'images/rebeca.png',
    jobTitle: 'Project Manager',
    stars: 4.6,
  ),
  Character(
    name: 'Ricard Ricart',
    age: 22,
    image: 'images/ricard.png',
    jobTitle: 'QA Team Lead',
    stars: 4.0,
  ),
  Character(
    name: 'Sílvia Salom',
    age: 27,
    image: 'images/silvia.png',
    jobTitle: 'DevOps Team Lead',
    stars: 3.9,
  ),
];
