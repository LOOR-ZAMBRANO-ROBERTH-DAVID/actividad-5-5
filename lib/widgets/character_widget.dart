// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:cartoons_flutter/model/character.dart';

class CharacterWidget extends StatelessWidget {
  final Character character;

  const CharacterWidget({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //TODO 0: Usa widgets básicos que ya conoces para crear este widget/cell.
    // Algunos consejos útiles:
    // * No elimine el widget Container(), agregue todos sus widgets dentro. Recuerde que el widget Container
    //   tiene un solo hijo, pero su primer paso probablemente debería ser agregar un widget Row como ese hijo.
    // * Cargue imágenes con Image.asset (character.image, ...
    // * En el widget Container, siéntase libre de usar decoration:
    //   BoxDecoration (color: Colors.black12, borderRadius: BorderRadius.all (Radius.circular (20.0)))
    return Container(
        margin: EdgeInsets.only(
          left: 10,
          top: 20,
          right: 10,
          bottom: 10,
        ),
        width: 250,
        height: 150,
        decoration: BoxDecoration(
            color: Colors.lightBlue,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.blue, width: 4),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.7),
                  spreadRadius: 5,
                  blurRadius: 2,
                  offset: Offset(0, 3))
            ]),
        padding: EdgeInsets.only(left: 10, top: 10),
        child: Row(
          children: [
            Container(
              child: Image.asset(character.image),
              height: 150,
            ),
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      character.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color:
                              character.stars < 4.0 ? Colors.red : Colors.green,
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          character.stars.toString(),
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        ),
                        padding: EdgeInsets.all(10),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          character.jobTitle,
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
